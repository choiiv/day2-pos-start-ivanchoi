package org.example;

public class OrderItem {
    public String barcode;
    public String name;
    public String unit;
    public Float quantity;
    public Float unitPrice;

    public OrderItem(String barcode, String name, String unit, Float quantity, Float unitPrice) {
        this.barcode = barcode;
        this.name = name;
        this.unit = unit;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }
}
