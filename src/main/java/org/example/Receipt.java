package org.example;

import java.util.Optional;

public class Receipt {
    public ShoppingList shoppingList;
    public Optional<FreeItemsList> freeItemsList;
    public ChargesList chargesList;

    public Receipt(ShoppingList shoppingList, Optional<FreeItemsList> freeItemsList, ChargesList chargesList) {
        this.shoppingList = shoppingList;
        this.freeItemsList = freeItemsList;
        this.chargesList = chargesList;
    }
}
