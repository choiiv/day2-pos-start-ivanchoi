package org.example;


import javax.swing.text.html.Option;
import java.text.DecimalFormat;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Printer {
    public String print(String[] cart, String[] promotions) {
        Map<String, String> allItems = PosDataLoader.loadAllItems();
        ShoppingList shoppingList = generateShoppingList(cart, allItems);
        Optional<FreeItemsList> freeItemsList = generateFreeItemsList(promotions, shoppingList);
        ChargesList chargesList = generateChargesList(shoppingList, freeItemsList);
        Receipt receipt = new Receipt(shoppingList, freeItemsList, chargesList);
        return format(receipt);
    }

    private String format(Receipt receipt) {
        String result = "***<No Profit Store> Shopping List***\n" + "----------------------\n";
        result += formatShoppingList(receipt.shoppingList);
        result += "----------------------\n";
        if(receipt.freeItemsList.isPresent()) {
            result += formatFreeItemsList(receipt.freeItemsList.get());
            result += "----------------------\n";
        }
        result += formatChargesList(receipt.chargesList);
        result += "**********************\n";
        return result;
    }

    private String formatChargesList(ChargesList chargesList) {
        String result = String.format("Total：%.2f(CNY)\n", chargesList.total);
        if(chargesList.saved.isPresent()) {
            result += String.format("Saved：%.2f(CNY)\n", chargesList.saved.get());
        }
        return result;
    }

    private String formatFreeItemsList(FreeItemsList freeItemsList) {
        List<String> orderItemsString = new ArrayList<>();
        freeItemsList.freeItems.forEach(freeItem -> {
            String unit = freeItem.quantity > 1 ? freeItem.unit + "s" : freeItem.unit;
            orderItemsString.add(String.format("Name：%s，Quantity：%s %s，Value：%.2f(CNY)\n",
                    freeItem.name, freeItem.quantity, unit, freeItem.totalPrice));
        });
        return "Buy two get one free items：\n" + String.join("", orderItemsString);
    }

    private String formatShoppingList(ShoppingList shoppingList) {
        List<String> orderItemsString = new ArrayList<>();
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        decimalFormat.setDecimalSeparatorAlwaysShown(false);
        shoppingList.orderItems.forEach(orderItem -> {
            String unit = orderItem.quantity > 1 ? orderItem.unit + "s" : orderItem.unit;
            orderItemsString.add(String.format("Name：%s，Quantity：%s %s，Unit Price：%.2f(CNY)，Subtotal：%.2f(CNY)\n",
            orderItem.name, decimalFormat.format(orderItem.quantity), unit, orderItem.unitPrice, orderItem.unitPrice * orderItem.quantity));
        });
        return String.join("", orderItemsString);
    }

    private ChargesList generateChargesList(ShoppingList shoppingList, Optional<FreeItemsList> freeItemsList) {
        Optional<Float> saved;
        Float total = shoppingList.orderItems.stream().map(orderItem -> orderItem.unitPrice * orderItem.quantity).reduce(0f, Float::sum);
        if(freeItemsList.isEmpty()) {
            saved = Optional.empty();
        }
        else {
            Float totalSaved = freeItemsList.get().freeItems.stream().map(freeItem -> freeItem.totalPrice).reduce(0f, Float::sum);
            saved = Optional.of(totalSaved);
            total -= totalSaved;
        }
        return new ChargesList(total, saved);
    }

    private FreeItem writeFreeItem(String name, String unit, Integer quantity, Float totalPrice) {
        return new FreeItem(name, unit, quantity, totalPrice);
    }


    private Optional<FreeItemsList> generateFreeItemsList(String[] promotions, ShoppingList shoppingList) {
        FreeItemsList freeItemsList = new FreeItemsList();
        shoppingList.orderItems.forEach(orderItem -> {
            if(orderItem.quantity >= 3 && Arrays.stream(promotions).toList().contains(orderItem.barcode)) {
                Integer quantity = (int) Math.floor(orderItem.quantity / 3);
                Float totalPrice = orderItem.unitPrice * quantity;
                freeItemsList.freeItems.add(writeFreeItem(orderItem.name, orderItem.unit, quantity, totalPrice));
            }
        });
        if(freeItemsList.freeItems.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(freeItemsList);
    }


    private ShoppingList generateShoppingList(String[] cart, Map<String, String> allItems) {
        List<Order> orders = aggregateItems(cart);
        List<DatabaseItem> databaseItems = parseDatabaseItems(allItems);

        ShoppingList shoppingList = new ShoppingList();
        orders.forEach(order -> {
            DatabaseItem targetDatabaseItem = databaseItems.stream().filter(databaseItem -> databaseItem.barcode.equals(order.barcode)).findAny().orElse(null);
            shoppingList.orderItems.add(generateOrderItem(order, targetDatabaseItem));
        });
        return shoppingList;
    }

    private OrderItem generateOrderItem(Order order, DatabaseItem databaseItem) {
        return new OrderItem(order.barcode, databaseItem.name, databaseItem.unit, order.quantity, databaseItem.unitPrice);
    }

    private List<DatabaseItem> parseDatabaseItems(Map<String, String> allItems) {
        List<DatabaseItem> databaseItems = new ArrayList<>();
        allItems.forEach((barcode, details) -> {
            String[] detailsList = details.split(",");
            System.out.println(detailsList[2]);
            databaseItems.add(new DatabaseItem(barcode, detailsList[0], Float.parseFloat(detailsList[1]), detailsList[2]));
        });
        return databaseItems;
    }

    private List<Order> aggregateItems(String[] cart) {
        List<Order> orders = new ArrayList<>();
        Arrays.stream(cart).forEach(cartItem -> {
            Boolean isWeighted = cartItem.contains("-");
            if(isWeighted) {
                int hypyenIndex = cartItem.indexOf('-');
                String barcode = cartItem.substring(0, hypyenIndex);
                Float quantity = Float.parseFloat(cartItem.substring(hypyenIndex+1));
                orders.add(new Order(barcode, quantity));
            }
            else {
                Order targetOrders = orders.stream().filter(order -> order.barcode.equals(cartItem)).findAny().orElse(null);
                if(targetOrders == null) {
                    orders.add(new Order(cartItem, 1f));
                }
                else {
                    targetOrders.quantity += 1f;
                }
            }
        });
        return orders;
    }
}

