package org.example;

public class DatabaseItem {
    public String barcode;
    public String name;
    public Float unitPrice;
    public String unit;

    public DatabaseItem(String barcode, String name, Float unitPrice, String unit) {
        this.barcode = barcode;
        this.name = name;
        this.unitPrice = unitPrice;
        this.unit = unit;
    }
}
