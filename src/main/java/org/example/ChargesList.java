package org.example;

import java.util.Optional;

public class ChargesList {
    public Float total;
    public Optional<Float> saved;

    public ChargesList(Float total, Optional<Float> saved) {
        this.total = total;
        this.saved = saved;
    }
}
