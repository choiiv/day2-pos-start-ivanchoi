package org.example;

public class FreeItem {
    public String name;
    public String unit;
    public Integer quantity;
    public Float totalPrice;

    public FreeItem(String name, String unit, Integer quantity, Float totalPrice) {
        this.name = name;
        this.unit = unit;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
    }
}
