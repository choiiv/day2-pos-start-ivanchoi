package org.example;

public class Order {
    public String barcode;
    public Float quantity;

    public Order(String barcode, Float quantity) {
        this.barcode = barcode;
        this.quantity = quantity;
    }
}
